package com.bookmyfurniture.ui.test;

import org.testng.annotations.Test;

import com.bookmyfurniture.ui.generalLibraries.BaseClass;
import com.bookmyfurniture.ui.generalLibraries.ObjectRepository;

public class homePageTest extends BaseClass{

	@Test(dataProvider="registerUserData",enabled=false)
	public void sampleTest(String userName, String number, String email, String password){
		initializeTestReporting("Home page login test");
		startNode("Registering User");
		ObjectRepository.homeStepsObj.registerInToApp(userName, number, email, password);
		stopNode();
		startNode("Validating user by signing in");
		ObjectRepository.homeStepsObj.signInWithRegisteredUser();
		stopNode();
	}
	
	@Test(groups={"tempGroup"})
	public void UiFlow() {
		initializeTestReporting("Home page login test");
		ObjectRepository.homeStepsObj.registerRandomTopApp();
		ObjectRepository.homeStepsObj.signInWithRegisteredUser();
		ObjectRepository.allFurnitureStepsObj.addItemToTheCart();
		ObjectRepository.allFurnitureStepsObj.placeOrderForCartItem();
	}
	
	@Test(groups={"part1"})
	public void UiFlowPart1() {
		initializeTestReporting("Home page login test");
		ObjectRepository.homeStepsObj.signInWithRegisteredUser("d21t@sdf.sdfs", "Test@1234");
		ObjectRepository.allFurnitureStepsObj.addItemToTheCart();
	}
	
	/*@Test(groups={"tempGroup"})
	public void sampleTest(){
		initializeTestReporting("Home page login test");
		startNode("Registering User");
		ObjectRepository.homeStepsObj.registerRandomTopApp();
		stopNode();
		startNode("Validating user by signing in");
		ObjectRepository.homeStepsObj.signInWithRegisteredUser();
		stopNode();
		startNode("addiding the item to cart");
		ObjectRepository.allFurnitureStepsObj.addItemToTheCart();
		stopNode();
		startNode("addiding the item to cart");
		ObjectRepository.allFurnitureStepsObj.verifyTheCartItemWithDB();
		stopNode();
		startNode("placing the order");
		ObjectRepository.allFurnitureStepsObj.placeOrderForCartItem();
		stopNode();
		startNode("verify the placed order with db");
		ObjectRepository.allFurnitureStepsObj.verifyPlacedOrderWithDB();
		stopNode();
	}*/
	
	
}


