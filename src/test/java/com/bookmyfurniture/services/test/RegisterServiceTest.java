package com.bookmyfurniture.services.test;

import org.testng.annotations.Test;

import com.bookmyfurniture.ui.generalLibraries.BaseClass;
import com.bookmyfurniture.ui.generalLibraries.ObjectRepository;

public class RegisterServiceTest extends BaseClass {

	@Test(groups={"registerServiceTest"})
	public void registerUserWithService() {
		initializeTestReporting("RegisterServiceTest");
		ObjectRepository.registerServiceObj.createRegisterReq("asdsa@sdfsd.sdf", "MALE", "7410852000", "etc",
				"Test@2134");
		ObjectRepository.registerServiceObj.verifyRegisterResponseWithDB();
	}

	@Test(groups={"registerUpdateServiceTest"})
	public void registerAndUpdateUserAddressWithService() {
		initializeTestReporting("RegisterUpdateUserAddrService");
		ObjectRepository.registerServiceObj.createRegisterReq("RegisterServiceData");
		ObjectRepository.registerServiceObj.verifyRegisterResponseWithDB();
		ObjectRepository.registerServiceObj.updateCreatedUserDataReq("UpdateUserData");
		ObjectRepository.registerServiceObj.verifyUpdateResponseWithDB();
	}

	@Test(groups={"deleteServiceTest"})
	public void verifyAndDeleteUserWithService() {
		initializeTestReporting("verifyAndDeleteUserWithService");
		ObjectRepository.registerServiceObj.verifyTotalProfilesWithDB();
		ObjectRepository.registerServiceObj.deleteCreatedUserFromServiceAndVerifyWithDb();
	}
}
