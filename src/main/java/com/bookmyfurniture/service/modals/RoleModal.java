package com.bookmyfurniture.service.modals;

public class RoleModal {

	String description;
	String name;
	int roleId;

	public RoleModal() {
		// TODO Auto-generated constructor stub
	}
	
	public RoleModal(String description, String name, int roleId) {
		this.description = description;
		this.name = name;
		this.roleId = roleId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

}
