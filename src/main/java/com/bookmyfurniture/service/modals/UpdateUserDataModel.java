package com.bookmyfurniture.service.modals;

public class UpdateUserDataModel {

	String addressCity;
	String addressLandMark;
	String addressLane1;
	String addressLane2;
	String addressState;
	String addressZipCode;
	String emailId;
	String gender;
	String mobileNo;
	String name;
	String password;
	RoleModal role;
	int userId;
	String userStatus;

	public UpdateUserDataModel(){
		
	}
	
	public UpdateUserDataModel(String addressCity, String addressLandMark, String addressLane1, String addressLane2,
			String addressState, String addressZipCode, String emailId, String gender, String mobileNo, String name,
			String password, RoleModal role, int userId, String userStatus) {
		super();
		this.addressCity = addressCity;
		this.addressLandMark = addressLandMark;
		this.addressLane1 = addressLane1;
		this.addressLane2 = addressLane2;
		this.addressState = addressState;
		this.addressZipCode = addressZipCode;
		this.emailId = emailId;
		this.gender = gender;
		this.mobileNo = mobileNo;
		this.name = name;
		this.password = password;
		this.role = role;
		this.userId = userId;
		this.userStatus = userStatus;
	}

	public String getAddressCity() {
		return addressCity;
	}

	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	public String getAddressLandMark() {
		return addressLandMark;
	}

	public void setAddressLandMark(String addressLandMark) {
		this.addressLandMark = addressLandMark;
	}

	public String getAddressLane1() {
		return addressLane1;
	}

	public void setAddressLane1(String addressLane1) {
		this.addressLane1 = addressLane1;
	}

	public String getAddressLane2() {
		return addressLane2;
	}

	public void setAddressLane2(String addressLane2) {
		this.addressLane2 = addressLane2;
	}

	public String getAddressState() {
		return addressState;
	}

	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}

	public String getAddressZipCode() {
		return addressZipCode;
	}

	public void setAddressZipCode(String addressZipCode) {
		this.addressZipCode = addressZipCode;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public RoleModal getRole() {
		return role;
	}

	public void setRole(RoleModal role) {
		this.role = role;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

}
