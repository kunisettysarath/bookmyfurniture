package com.bookmyfurniture.service.modals;

public class RegisterUserModal {

	String emailId;
	String gender;
	String mobileNo;
	String name;
	String password;
	RoleModal role;
	String userStatus;

	public RegisterUserModal() {
		
	}

	public RegisterUserModal(String emailId, String gender, String mobileNo, String name, String password,
			RoleModal role, String userStatus) {
		super();
		this.emailId = emailId;
		this.gender = gender;
		this.mobileNo = mobileNo;
		this.name = name;
		this.password = password;
		this.role = role;
		this.userStatus = userStatus;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public RoleModal getRole() {
		return role;
	}

	public void setRole(RoleModal role) {
		this.role = role;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

}
