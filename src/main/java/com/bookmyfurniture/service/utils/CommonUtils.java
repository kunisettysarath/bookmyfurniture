package com.bookmyfurniture.service.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

import org.json.JSONObject;

import com.bookmyfurniture.common.PropertyUtil;

public class CommonUtils {

	
	@SuppressWarnings("unused")
	private JSONObject createRegisterDataJson(String emaild, String gender, String mobileNo, String name,
			String password) {
		JSONObject reqJsonObj = new JSONObject();
		reqJsonObj.put("emailId", emaild);
		reqJsonObj.put("gender", gender);
		reqJsonObj.put("mobileNo", mobileNo);
		reqJsonObj.put("name", name);
		reqJsonObj.put("password", password);
		JSONObject role = new JSONObject();
		role.put("description", "string");
		role.put("name", "user");
		role.put("roleId", 2);
		reqJsonObj.put("role", role);
		reqJsonObj.put("userStatus", "ACTIVE");
		return reqJsonObj;
	}
	
	public String generateStringFromResource(String path) {
		String s = null;
		try {
			s = new String(
					Files.readAllBytes(Paths.get(PropertyUtil.getData("ServiceTestResourcePath") + path + ".json")));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return s;
	}
	
	public String randomStringGenerator(int size, String randomType) {
		String randomStringChars = null;
		if (randomType.equalsIgnoreCase("text"))
			randomStringChars = "abcdefghijklmnopqrstuvwxyz";
		else if (randomType.equalsIgnoreCase("number"))
			randomStringChars = "1234567890";
		else
			randomStringChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		StringBuilder builder = new StringBuilder();
		Random rnd = new Random();
		while (builder.length() < size) { // length of the random string.
			int index = (int) (rnd.nextFloat() * randomStringChars.length());
			builder.append(randomStringChars.charAt(index));
		}
		return builder.toString();
	}

}
