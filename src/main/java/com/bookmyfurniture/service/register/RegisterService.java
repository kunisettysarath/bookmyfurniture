package com.bookmyfurniture.service.register;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.testng.Assert;

import com.bookmyfurniture.common.PropertyUtil;
import com.bookmyfurniture.common.Reporter;
import com.bookmyfurniture.common.TestConstants;
import com.bookmyfurniture.service.modals.RegisterUserModal;
import com.bookmyfurniture.service.modals.RoleModal;
import com.bookmyfurniture.service.modals.UpdateUserDataModel;
import com.bookmyfurniture.ui.generalLibraries.ObjectRepository;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RegisterService extends TestConstants {
	RequestSpecification request = null;
	Reporter report =null;
	ObjectMapper mapper = new ObjectMapper();

	public RegisterService(Reporter report) {
		this.report = report;
	}
	
	public RequestSpecification setBaseUriAndPath(String basePath) {
		request = RestAssured.given();
		report.addInfoInReport("base url set ----> "+PropertyUtil.getData("BaseURL"));
		report.addInfoInReport("base path set ----> "+basePath);
		report.addInfoInReport("contentType ----> "+APPLICATION_JSON);
		return request.baseUri(PropertyUtil.getData("BaseURL")).contentType(APPLICATION_JSON).basePath(basePath);
	}

	public void createRegisterReq(String fileName) {
		String payLoad = ObjectRepository.commonUtils.generateStringFromResource(fileName);
		request = setBaseUriAndPath(PropertyUtil.getData("SignUpEndPoint"));
		report.addInfoInReport("payload -->"+payLoad);
		registerResponse = request.body(payLoad).when().post();
		report.addInfoInReport("response obtained -->"+registerResponse.asString());
		try {
			RegisterUserModal readValue = mapper.readValue(payLoad, RegisterUserModal.class);
			testData.put("registerUserModal", readValue);
		} catch (IOException e) {
			e.printStackTrace();
		}
		report.addInfoInReport("status code of the response --->"+registerResponse.getStatusCode());
		registerResponse.prettyPrint();

	}

	public void createRegisterReq(String emailId, String gender, String mobileNo, String name, String password) {
		request = setBaseUriAndPath(PropertyUtil.getData("SignUpEndPoint"));
		RegisterUserModal regUser = new RegisterUserModal(emailId, gender, mobileNo, name, password,
				new RoleModal("string", "user", 2), "ACTIVE");
		testData.put("registerUserModal", regUser);
		registerResponse = request.body(regUser).when().post();
		registerResponse.prettyPrint();
	}

	public void updateCreatedUserDataReq(String fileName) {
		request = setBaseUriAndPath(PropertyUtil.getData("UpdateProfileEndPoint"));
		String payLoad = ObjectRepository.commonUtils.generateStringFromResource(fileName);
		payLoad = StringUtils.chop(payLoad) + ",\"userId\": " + testData.get("userDataId") + "}";
		try {
			UpdateUserDataModel readValue = mapper.readValue(payLoad, UpdateUserDataModel.class);
			testData.put("updateUserDataModel", readValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		report.addInfoInReport("Payload  -->"+payLoad);
		updateProfileResponse = request.body(payLoad).when().put();
		report.addInfoInReport("response code  -->"+updateProfileResponse.getStatusCode());
		report.addInfoInReport("response obtained  -->"+updateProfileResponse.asString());
		report.addInfoInReport("response obtained  -->"+updateProfileResponse.prettyPrint());
		updateProfileResponse.prettyPrint();
		
	}

	public void verifyRegisterResponseWithDB() {
		RegisterUserModal storedUser = (RegisterUserModal) testData.get("registerUserModal");
		List<String> userDataStored = ObjectRepository.dataBaseUtil.getUserData("email", storedUser.getEmailId());
		testData.put("userDataId", userDataStored.get(0));
		Assert.assertEquals(userDataStored.get(9), storedUser.getMobileNo(), "Mobile number not matching");
		report.addInfoInReport("Mobile number of the created user verified successfully");
		Assert.assertEquals(userDataStored.get(10), storedUser.getName(), "name not matching");
		report.addInfoInReport("name of the created user verified successfully");
	}
	
	public void verifyUpdateResponseWithDB() {
		UpdateUserDataModel storedUser = (UpdateUserDataModel) testData.get("updateUserDataModel");
		List<String> userDataStored = ObjectRepository.dataBaseUtil.getUserData("userId", String.valueOf(storedUser.getUserId()));
		Assert.assertEquals(userDataStored.get(1), storedUser.getAddressCity(), "address city not matching not matching");
		report.addInfoInReport("Address city verified successfully");
		Assert.assertEquals(userDataStored.get(2), storedUser.getAddressLandMark(), "address landmark not matching");
		report.addInfoInReport("address landmark verified successfully");
		Assert.assertEquals(userDataStored.get(3), storedUser.getAddressLane1(), "address lane 1 not matching");
		report.addInfoInReport("address lane 1 verified successfully");
		Assert.assertEquals(userDataStored.get(4), storedUser.getAddressLane2(), "address lane 2 not matching");
		report.addInfoInReport("address lane 2 verified successfully");
		Assert.assertEquals(userDataStored.get(5), storedUser.getAddressState(), "address state not matching");
		report.addInfoInReport("address state verified successfully");
		Assert.assertEquals(userDataStored.get(6), storedUser.getAddressZipCode(), "zip code not matching");
		report.addInfoInReport("zip code verified successfully");
		Assert.assertEquals(userDataStored.get(7), storedUser.getEmailId(), "email id not matching");
		report.addInfoInReport("email id verified successfully");
		Assert.assertEquals(userDataStored.get(9), storedUser.getMobileNo(), "mobile number not matching");
		report.addInfoInReport("mobile number verified successfully");
		Assert.assertEquals(userDataStored.get(10), storedUser.getName(), "name not matching");
		report.addInfoInReport("name verified successfully");
	}

	public void verifyTotalProfilesWithDB() {
		request = setBaseUriAndPath(PropertyUtil.getData("TotalProfilesCreatedEndPoint"));
		String totalProfiles = String.valueOf(request.when().get().then().extract().response().jsonPath().getMap("body")
				.get("userCount"));
		report.addInfoInReport("total profiles obtained from the response---->"+totalProfiles);
		Assert.assertEquals(totalProfiles, ObjectRepository.dataBaseUtil.getTotalRecordsInTheTable("profile"));
		report.addInfoInReport("total profiles matched successfully");
	}

	public void deleteCreatedUserFromServiceAndVerifyWithDb() {
		request = setBaseUriAndPath(PropertyUtil.getData("DeleteProfileEndPoint"));
		String idToDelete = testData.get("userDataId").toString();
		Response response = request.pathParam("id", idToDelete).when().delete().then().extract().response();
		report.addInfoInReport("response obtained----->"+response.prettyPrint());
		response.then().assertThat().statusCode(200);
		Assert.assertEquals(response.jsonPath().getString("body"), " Successfully deleted profile - "+idToDelete);
		report.addInfoInReport("user deletion verifed...");
	}

	
}





/*public void temp() {

RequestSpecification request = RestAssured.given().contentType("application/json");
request.baseUri(PropertyUtil.getData("BaseURL"));
request.body("{" + "  \"emailId\": \"eerr@ere.ert\"," + "  \"gender\": \"MALE\","
		+ "  \"mobileNo\": \"1010101010\"," + "  \"name\": \"damn\"," + "  \"password\": \"Test@1234\","
		+ "  \"role\": {" + "    \"description\": \"string\"," + "    \"name\": \"user\"," + "    \"roleId\": 2"
		+ "  }," + "  \"userStatus\": \"ACTIVE\"" + "}");
request.basePath(PropertyUtil.getData("SignUpEndPoint"));

RestAssured.given().baseUri("http://okmry52647dns.eastus.cloudapp.azure.com:8089")
		.contentType("application/json")
		.body("{" + "  \"emailId\": \"string\"," + "  \"gender\": \"MALE\"," + "  \"mobileNo\": \"string\","
				+ "  \"name\": \"Soumya, Harish, etc..\"," + "  \"password\": \"string\"," + "  \"role\": {"
				+ "    \"description\": \"string\"," + "    \"name\": \"Admin, user, etc..\","
				+ "    \"roleId\": 1234" + "  }," + "  \"userStatus\": \"ACTIVE\"" + "}")
		.post("/rest/api/signup/").body().prettyPrint();

Response res;
res = RestAssured.given().contentType("application/json")
		.baseUri("http://okmry52647dns.eastus.cloudapp.azure.com:8089")
		.body(generateStringFromResource(
				"C:\\Users\\kunis\\Downloads\\Project\\Repo\\bookmyfurniture\\src\\main\\resources\\com\\bookmyfurniture\\service\\NewFile.json"))
		.post("/rest/api/signup/");

}*/