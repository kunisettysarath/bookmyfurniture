package com.bookmyfurniture.service.register;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.bookmyfurniture.ui.generalLibraries.InitializeBrowser;

public class temp{

	static WebDriver driver = null;
	public static void main(String[] args) {
		 
		 driver = new InitializeBrowser(driver).getDriver("chrome");
		 driver.get("http://okmry52647dns.eastus.cloudapp.azure.com:9090/");
		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		 driver.manage().window().maximize();
		 driver.findElement(By.xpath("//*[@id='navbarSupportedContent']/form/button[4]")).click();
		 driver.findElement(By.id("emailId")).sendKeys("temp@temp1.com1");;
		 driver.findElement(By.id("password")).sendKeys("Test@1234");
		 driver.findElement(By.xpath("//*[@type='submit']")).click();
		 waits(1000);
		 driver.findElement(By.xpath("//*[text()=' All Furnitures ']")).click();
		 waitExplicit(driver.findElement(By.xpath("//*[text()=' Furnitures']")), 30);
		 driver.findElement(By.xpath("/html/body/app-root/bmf-layout/div[2]/product-catalogue/div/div/div/div[3]/div[2]/div/div[1]/div/button")).click();
		 waitExplicit(driver.findElement(By.xpath("//*[text()='Buy Now ']")), 30);
		 driver.findElement(By.xpath("//*[text()='Add to Cart ']")).click();
		 Assert.assertTrue(driver.findElement(By.xpath("//*[text()=' Successfully added product to your cart ']")).isDisplayed());
		 driver.findElement(By.xpath("//*[text()=' Cart ']")).click();
		 Assert.assertTrue(driver.findElement(By.xpath("//*[starts-with(@data-icon,'shopping')]")).isDisplayed());
		 driver.findElement(By.xpath("//*[text()='Delete']")).click();
		 //driver.findElement(By.xpath("//*[text()='Place Order']")).click();
	 	waitExplicit(driver.findElement(By.xpath("//*[text()=' Are you sure you want to delete this product? ']")), 30);
		 System.out.println(driver.findElement(By.xpath("//*[text()='Yes']")).isDisplayed());
		 System.out.println(driver.findElement(By.xpath("//*[text()=' Are you sure you want to delete this product? ']")).isDisplayed());
		 driver.findElement(By.xpath("//*[text()='Yes']")).click();
	
	}
	
	public static void waits(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void waitExplicit(WebElement element, int timeOut){
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
}