package com.bookmyfurniture.common;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.bookmyfurniture.ui.utils.ScreenshotUtil;

public class Reporter {
	ExtentReports extent;
	ExtentTest test;
	ExtentTest nodeTest;

	public void configReporter() {
		ExtentHtmlReporter report = new ExtentHtmlReporter("./build/target/ExtentReports/extentReport.html");
		File dir = new File("./build/target/ExtentReports");
		if (!dir.exists())
			dir.mkdirs();
		extent = new ExtentReports();
		extent.attachReporter(report); 
		report.config().setTheme(Theme.DARK);
	}

	public void startTest(String name) {
		test = extent.createTest(name);
	}

	public void startNodeTest(String name) {
		nodeTest = test.createNode(name);
	}

	public void addInfoInReport(String infoMsg) {
		if (TestConstants.REPORTING_NODE)
			nodeTest.log(Status.INFO, infoMsg);
		else
			test.log(Status.INFO, infoMsg);
	}

	public void addErrorInReport(String errorMsg) {
		if (TestConstants.REPORTING_NODE)
			nodeTest.log(Status.ERROR, errorMsg);
		else
			test.log(Status.ERROR, errorMsg);
	}

	public void createNode(String nodeName) {
		nodeTest = test.createNode(nodeName);
		TestConstants.REPORTING_NODE = true;
	}

	public void killNode() {
		TestConstants.REPORTING_NODE = false;
	}

	public void addExtraInfoToReport(String name, String value) {
		extent.setSystemInfo(name, value);
	}

	public void testFailed(String message) {
		try {
			if (TestConstants.REPORTING_NODE)
			test.fail(message,
					MediaEntityBuilder.createScreenCaptureFromBase64String(ScreenshotUtil.takeScreenshotAsBase64()).build());
			else
				test.fail(message,
						MediaEntityBuilder.createScreenCaptureFromBase64String(ScreenshotUtil.takeScreenshotAsBase64()).build());		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void testSkipped(String message) {
		try {
			if (TestConstants.REPORTING_NODE)
				nodeTest.skip(message, MediaEntityBuilder
						.createScreenCaptureFromPath(ScreenshotUtil.takeScreenshot("/Fail/")).build());
			else
				test.skip(message, MediaEntityBuilder
						.createScreenCaptureFromPath(ScreenshotUtil.takeScreenshot("/Fail/")).build());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void serviceStepPassed(String message) {
		if (TestConstants.REPORTING_NODE)
			nodeTest.pass(message);
		else
			test.pass(message);
	}

	public void serviceStepFailed(String message) {
		if (TestConstants.REPORTING_NODE)
			nodeTest.fail(message);
		else
			test.fail(message);
	}

	public void serviceStepSkipped(String message) {
		if (TestConstants.REPORTING_NODE)
			nodeTest.skip(message);
		else
			test.skip(message);
	}

	public void stepPassed(String message) {
		String messageSplit[] = message.replace("By.", "->").split("->");
		try {
			if (!TestConstants.REPORTING_NODE)
				test.pass(messageSplit[0] + " " + StringUtils.chop(messageSplit[2]),
						MediaEntityBuilder.createScreenCaptureFromBase64String(ScreenshotUtil.takeScreenshotAsBase64()).build());
			else
				nodeTest.pass(messageSplit[0] + " " + StringUtils.chop(messageSplit[2]), MediaEntityBuilder
						.createScreenCaptureFromPath(ScreenshotUtil.takeScreenshotAsBase64()).build());
		} catch (ArrayIndexOutOfBoundsException e) {
			try {
				if (TestConstants.REPORTING_NODE)
					nodeTest.pass(message, MediaEntityBuilder
							.createScreenCaptureFromPath(ScreenshotUtil.takeScreenshotAsBase64()).build());
				else
					test.pass(message,
							MediaEntityBuilder.createScreenCaptureFromBase64String(ScreenshotUtil.takeScreenshotAsBase64()).build());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	public void stopReporting() {
		extent.flush();
	}
}

/*
 * ExtentHtmlReporter avent = new
 * ExtentHtmlReporter("./ExtentReports/temp.html"); ExtentReports extent = new
 * ExtentReports(); extent.attachReporter(avent);
 * 
 * ExtentTest test = extent.createTest("sdfdsfg","description"); ExtentTest node
 * = test.createNode("sdfdsfg","description"); test.log(Status.INFO,
 * "abt to start");
 * 
 * 
 * node.log(Status.INFO, "abt to start"); extent.setSystemInfo("browser: ",
 * "chrome"); extent.setSystemInfo("env: ", "qa1"); Markup m1 =
 * MarkupHelper.createLabel ("sdsdfsd", ExtentColor.CYAN); String[][] data = { {
 * "Header1", "Header2", "Header3" }, { "Content.1.1", "Content.2.1",
 * "Content.3.1" }, { "Content.1.2", "Content.2.2", "Content.3.2" }, {
 * "Content.1.3", "Content.2.3", "Content.3.3" }, { "Content.1.4",
 * "Content.2.4", "Content.3.4" } }; Markup m = MarkupHelper.createTable(data);
 * 
 * test.log(Status.INFO, m); test.log(Status.INFO, m1);
 */

/*
 * test.log(Status.INFO, "ended"); node.log(Status.INFO, "ended");
 * extent.flush();
 */