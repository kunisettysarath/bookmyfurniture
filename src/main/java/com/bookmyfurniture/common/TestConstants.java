package com.bookmyfurniture.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.restassured.response.Response;

public class TestConstants {

	// Common test constants
	public static final int TIMEOUT_PERIOD_MINIMUM = 5;
	public static final int TIMEOUT_PERIOD_MEDIUM = 10;
	public static final int TIMEOUT_PERIOD_SHORT = 15;
	public static final int TIMEOUT_PERIOD_LONG = 30;
	public static final int TIMEOUT_PERIOD_MAX = 90;

	// Services test constants

	public static final String COOKIE = "Cookie";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String APPLICATION_JSON = "application/json";
	public static Response registerResponse = null;
	public static Response updateProfileResponse = null;
	public static Map<String, Object> testData = new ConcurrentHashMap<>();

	// UI test constants

	public static boolean REPORTING_NODE = false;

}
