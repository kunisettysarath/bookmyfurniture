package com.bookmyfurniture.ui.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.bookmyfurniture.common.PropertyUtil;

public class ExcelUtil {

	public FileInputStream fis = null;
	public FileOutputStream fos = null;
	public XSSFWorkbook workbook = null;
	public XSSFSheet sheet = null;
	public XSSFRow row = null;
	public XSSFCell cell = null;
	String sheetName;

	public ExcelUtil(String excelName, String sheetName) {
		try {
			fis = new FileInputStream(PropertyUtil.getData(excelName));
			workbook = new XSSFWorkbook(fis);
			fis.close();
			this.sheetName = sheetName;
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		new ExcelUtil("registerUserDataExcel", "RegisterData").getTotalExcelData();
	}

	@SuppressWarnings("static-access")
	public Object[][] getTotalExcelData() {
		sheet = workbook.getSheet(sheetName);
		String[][] data = new String[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		data[0][0] = "sdfds";
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			row = sheet.getRow(i);
			for (int j = 0; j < row.getLastCellNum(); j++) {
				cell = row.getCell(j);
				cell.setCellType(cell.CELL_TYPE_STRING);
				data[i - 1][j] = cell.getStringCellValue();
			}

		}
		return data;
	}
}
