package com.bookmyfurniture.ui.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.bookmyfurniture.common.PropertyUtil;
import com.mysql.cj.protocol.Resultset;
import com.mysql.cj.xdevapi.Result;

public class DataBaseUtil extends PropertyUtil {
	Connection con;
	Logger log = Logger.getLogger(DataBaseUtil.class);

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		DataBaseUtil db = new DataBaseUtil();
		db.connect();
		db.closeConnection();
	}

	public void connect() throws ClassNotFoundException, SQLException {
		log.info("database connection instantiated");
		try {
			Class.forName(getData("mysqlDriver"));
			con = DriverManager.getConnection(getData("dbUrl"), getData("username"), getData("password"));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(ExceptionUtils.getStackTrace(e));
			log.info("database connection error");
		}

	}

	public void insertData(String query) throws ClassNotFoundException, SQLException {
		connect();
		PreparedStatement pStmt = con.prepareStatement(query);
		pStmt.executeQuery();
	}
	
	public ArrayList<String> getProductData(String id){
		ArrayList<String> productData = new ArrayList<String>();
		ResultSet result = selectData("select product_id from cart where profile_user_id='"+id+"'");
		try {
			if(result.next())
				result = selectData("SELECT name, description FROM product where id='"+result.getInt(1)+"'");
			if(result.next())
				productData.add(result.getString(1));
			productData.add(result.getString(2));
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return productData;
	}
	public String getOrderUserId(String orderId){
		String userID = null;
		ResultSet result = selectData("SELECT user_user_id FROM product_order where order_reference='"+orderId+"'");
		try {
			if(result.next())
				userID = result.getString(1);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return userID;
	}

	public ResultSet selectData(String query) {
		ResultSet result = null;
		try {
			connect();
			PreparedStatement pStmt = con.prepareStatement(query);
			result = pStmt.executeQuery();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return result;
	}

	public ArrayList<String> getUserData(String columnName, String columnValue) {
		ArrayList<String> userData = null;
		try {
			connect();
		userData = new ArrayList<String>();
		ResultSet result = null;
			if(columnName.equalsIgnoreCase("email"))
				result = selectData("SELECT * FROM profile where email_id='" + columnValue + "'");
			else if(columnName.equalsIgnoreCase("userId"))
				result = selectData("SELECT * FROM profile where user_id='" + columnValue + "'");
			ResultSetMetaData metaData = result.getMetaData();
			while (result.next()) {
				for (int i = 1; i <= metaData.getColumnCount(); i++)
					userData.add(result.getString(i));
			
			closeConnection();
			}
		} catch (Exception e1) {
			
			e1.printStackTrace();
		} 
		return userData;
	}
	
	public String getTotalRecordsInTheTable(String tableName){
		String totalSize=null;
		try {
			connect();
			ResultSet result = selectData("SELECT count(*) FROM "+tableName);
			if(result.next())
				totalSize = result.getString(1);
			closeConnection();
		} catch (Exception e1) {
			
			e1.printStackTrace();
		} 
		return totalSize;
		
	}

	public void closeConnection() throws SQLException {
		if (true) {
			try {
				con.close();
				log.info("database connection close successfull");
			} catch (Exception e) {
				e.printStackTrace();
				log.error("database connection close error");
			}
		}
	}
}