package com.bookmyfurniture.ui.utils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class ScreenshotUtil {
	static WebDriver driver = null;

	public ScreenshotUtil(WebDriver driver) {
		ScreenshotUtil.driver = driver;
	}
	
	public static String takeScreenshot(String path){
		String screenshotLocation = System.getProperty("user.dir")+"/build/target/ScreenShots"+path+UUID.randomUUID().toString()+".jpg";

		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(screenshotLocation));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return screenshotLocation;

	}
	
	public static String takeScreenshotAsBase64() {
		String screenshotLocation = System.getProperty("user.dir")+"/build/target/ScreenShots/"+UUID.randomUUID().toString()+".jpg";
		String scrBase64 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);
		File file = OutputType.FILE.convertFromBase64Png(scrBase64);
		try {
			FileUtils.copyFile(file, new File(screenshotLocation), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return scrBase64;
	}

	public static String takeScreenShotUsingAshot() {
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
		String screenshotLocation = "./ScreenShots/"+UUID.randomUUID().toString()+".jpg";
		try {
			ImageIO.write(screenshot.getImage(), "jpg", new File(screenshotLocation));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return screenshotLocation;

	}

}
