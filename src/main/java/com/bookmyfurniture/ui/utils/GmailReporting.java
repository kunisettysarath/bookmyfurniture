package com.bookmyfurniture.ui.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.bookmyfurniture.common.PropertyUtil;

public class GmailReporting extends PropertyUtil {

	Logger log = Logger.getLogger(GmailReporting.class);
	
	public void sendMailOnRequest(){
		if(getData("SendMailAfterExecution").equalsIgnoreCase("yes"))
			sendMailOnRequest();
		else
			log.info("automatic mail trigger option is not enabled.....Please change the config file if needed!!");
	}

	public void sendReportOnMail() {
		log.info("initializing to send the report on mail to--->"+getData("RecipientEmailId"));
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		try {

			Session session = Session.getInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(getData("SenderEmailUserName"), getData("senderEmailPassword"));
				}
			});
			SimpleDateFormat gmtDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			gmtDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			Message message = new MimeMessage(session);

			message.setSubject("Report for execution on " + gmtDateFormat.format(new Date()));
			message.setFrom(new InternetAddress(getData("SenderEmailUserName")));
			String filename = System.getProperty("user.dir") + "\\build\\ExtentReports\\extentReport.html";
			DataSource source = new FileDataSource(filename);
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(getData("RecipientEmailId")));
			Transport.send(message);
			log.info("Report has been sent successfully to--->"+getData("RecipientEmailId"));

		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while sending the report on email");
		}
	}
}
