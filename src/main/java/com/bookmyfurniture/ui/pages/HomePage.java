package com.bookmyfurniture.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.bookmyfurniture.common.Reporter;
import com.bookmyfurniture.ui.generalLibraries.ActionHelper;

public class HomePage extends ActionHelper{
	
	public WebDriver driver = null;
	public Reporter report = null;
	
	public HomePage(WebDriver driver, Reporter report) {
		super(driver, report);
		this.driver = driver;
		this.report = report;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[@id=\"navbarSupportedContent\"]/form/button[4]/span")
	public WebElement signInButton;
	
	@FindBy(xpath = "//*[@class=\"btn btn-primary btn-md\"]")
	public WebElement createAccountButton;
	
	@FindBy(id = "name")
	public WebElement userNameInputField;
	
	@FindBy(id = "mobileNo")
	public WebElement mobileNoInputField;
	
	@FindBy(xpath = "/html/body/app-root/bmf-layout/div[2]/app-login/div/p-sidebar/div/app-signup/div/div/form/div[3]/input")
	public WebElement emailIdInputField;
	
	@FindBy(xpath = "/html/body/app-root/bmf-layout/div[2]/app-login/div/p-sidebar/div/app-signup/div/div/form/div[4]/div[1]/input")
	public WebElement passwordInputField;
	
	@FindBy(xpath = "/html/body/app-root/bmf-layout/div[2]/app-login/div/p-sidebar/div/app-signup/div/div/form/div[5]/div[1]/button")
	public WebElement registerButton;
	
	@FindBy(xpath = "/html/body/app-root/bmf-layout/div[2]/app-login/div/div/h2")
	public WebElement signInpageHeader;
	
	@FindBy(id = "emailId")
	public WebElement signInEmailField;
	
	@FindBy(id = "password")
	public WebElement signInPasswordField;
	
	@FindBy(xpath = "//*[@type='submit']")
	public WebElement signInSubmitButton;
	
	@FindBy(xpath = "//*[@id='navbarSupportedContent']/form/button[4]/span")
	public WebElement loggedInMessage;

	@FindBy(xpath = "/html/body/app-root/bmf-layout/div[2]/app-homepage/div/home-category-list/div[1]/div/h2")
	public WebElement browserFromCategoryText;
}