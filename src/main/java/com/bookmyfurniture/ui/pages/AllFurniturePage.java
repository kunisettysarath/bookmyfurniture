package com.bookmyfurniture.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.bookmyfurniture.common.Reporter;
import com.bookmyfurniture.ui.generalLibraries.ActionHelper;

public class AllFurniturePage extends ActionHelper{
	
	public WebDriver driver = null;
	public Reporter report = null;
	
	public AllFurniturePage(WebDriver driver, Reporter report) {
		super(driver, report);
		this.driver = driver;
		this.report = report;
		PageFactory.initElements(driver, this);
	}
	
//	private static ByXPath homeLableHeading = new ByXPath("//a[@href='']/text()");
//	private static ByXPath furnitureLableHeading = new ByXPath("//a[@href='furniture/all']/text()");
//	private static ByXPath furnituresSideBarHeading = new ByXPath("//*[@id='sticky-sidebar']/h2");
//	private static ByXPath categorySideBarHeading = new ByXPath("//*[@id='sticky-sidebar']/h3");
//	private static ByXPath chairCategoryCheckBox = new ByXPath("//*[@id='mat-checkbox-8-input']/../*[@class='mat-checkbox-frame']");
//	private static ByXPath sofaCategoryCheckBox = new ByXPath("//*[@id='mat-checkbox-9-input']/../*[@class='mat-checkbox-frame']");
//	private static ByXPath categorySelectedCloseButton = new ByXPath("//*[@id='mat-chip-list-1']/div/mat-chip/mat-icon");
//	private static ByXPath categorySelected = new ByXPath("//*[@id='mat-chip-list-1']/div/mat-chip/text()");
	

	@FindBy(xpath = "//*[text()=' All Furnitures ']")
	protected  WebElement allFurnituresButton;
	
	@FindBy(xpath = "//*[text()='Buy Now ']")
	protected  WebElement buyNowBtn;
	
	@FindBy(xpath = "/html/body/app-root/bmf-layout/div[2]/product-catalogue/div/div/div/div[3]/div[2]/div/div[1]/div/button")
	protected  WebElement firstItemAvailable;
	
	@FindBy(xpath = "/html/body/app-root/bmf-layout/div[2]/product-catalogue/div/div/div/div[3]/div[2]/div/div[2]/div/button")
	protected  WebElement secondItemAvailable;
	
	@FindBy(xpath = "//*[text()='Add to Cart ']")
	protected  WebElement addToCartBtn;
	
	@FindBy(xpath = "//*[text()='Add to Wishlist ']")
	protected  WebElement addToWishListBtn;
	
	@FindBy(xpath = "//*[text()=' Successfully added product to your cart ']")
	protected  WebElement itemAddedToCartMsg;
	
	@FindBy(xpath = "//*[text()=' Cart ']")
	protected  WebElement headerCartBtn;
	
	@FindBy(xpath = "//*[starts-with(@data-icon,'shopping')]")
	protected  WebElement myCartPageHeader;
	
	@FindBy(xpath = "//*[text()='Delete']")
	protected  WebElement cartDeleteBtn;
	
	@FindBy(xpath = "//*[text()='Place Order']")
	protected  WebElement placeOrderBtn;
	
	@FindBy(xpath = "//*[text()=' Are you sure you want to delete this product? ']")
	protected  WebElement deleteCnfrmationPopUpMsg;
	
	@FindBy(xpath = "//*[text()='Yes']")
	protected  WebElement deleteCartPopUpYesBtn;
	
	@FindBy(xpath = "//*[text()='No']")
	protected  WebElement deleteCartPopUPNoBtn;
	
	@FindBy(xpath = "/html/body/app-root/bmf-layout/div[2]/selected-product/div/div[1]/div[1]/div[2]/div[1]/div[1]/h1")
	protected  WebElement itemSelectedName;
	
	@FindBy(xpath = "//*[text()='Description']/../../*[2]")
	protected  WebElement itemSelectedDescription;
	
	@FindBy(xpath = "//*[text()='Select Payment Option']")
	protected  WebElement selectPaymentOptionText;
	
	@FindBy(xpath = "//*[text()='Cash']/../*[1]")
	protected  WebElement cashPaymentRadioButton;
	
	@FindBy(xpath = "//*[text()=' Place Order ']")
	protected  WebElement paymentPagePlaceOrderButton;
	
	@FindBy(xpath = "/html/body/app-root/bmf-layout/div[2]/cart-layout/div/app-payment-layout/div/p-card/div/div/div/div/h3")
	protected  WebElement orderReferenceNum;
	
	@FindBy(xpath = "//*[text()='Your order is successfully placed']")
	protected  WebElement orderSuccessfullText;
	
	
}
