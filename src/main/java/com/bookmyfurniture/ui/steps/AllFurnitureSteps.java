package com.bookmyfurniture.ui.steps;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.bookmyfurniture.common.Reporter;
import com.bookmyfurniture.common.TestConstants;
import com.bookmyfurniture.ui.generalLibraries.ObjectRepository;
import com.bookmyfurniture.ui.pages.AllFurniturePage;
import com.mysql.cj.x.protobuf.MysqlxDatatypes.Array;

public class AllFurnitureSteps extends AllFurniturePage {

	public AllFurnitureSteps(WebDriver driver, Reporter report) {
		super(driver, report);
	}
	
	public void addItemToTheCart(){
		clickOnElement(allFurnituresButton, "clicking on all furnitures button");
		Assert.assertTrue(isElementExists(firstItemAvailable, TestConstants.TIMEOUT_PERIOD_LONG));
		clickOnElement(secondItemAvailable, "clicking on all first item on the list");
		Assert.assertTrue(isElementExists(addToCartBtn, TestConstants.TIMEOUT_PERIOD_LONG));
		Assert.assertTrue(isElementExists(buyNowBtn));
		Assert.assertTrue(isElementExists(addToWishListBtn));
		TestConstants.testData.put("selectedItemName", getElementText(itemSelectedName));
		TestConstants.testData.put("selectedItemDescription", getElementText(itemSelectedDescription));
		clickOnElement(addToCartBtn, "clicking on add to cart button");
		Assert.assertTrue(isElementExists(itemAddedToCartMsg));
		waitForElementNotVisible(itemAddedToCartMsg);
		clickOnElement(headerCartBtn, "clicking on cart button");
		Assert.assertTrue(isElementExists(cartDeleteBtn, TestConstants.TIMEOUT_PERIOD_LONG));
		clickOnElement(cartDeleteBtn, "clicking on cart button");
		Assert.assertTrue(isElementExists(deleteCnfrmationPopUpMsg, "verifying delete confirmation popup"));
		Assert.assertTrue(isElementExists(deleteCartPopUpYesBtn));
		clickOnElement(deleteCartPopUPNoBtn, "clicking on NO button");
	}

	public void verifyTheCartItemWithDB() {
		// TODO Auto-generated method stub
		String userId = ObjectRepository.dataBaseUtil.getUserData("email", TestConstants.testData.get("registerUserEmailId").toString()).get(0);
		TestConstants.testData.put("userId", userId);
		ArrayList<String> productData = ObjectRepository.dataBaseUtil.getProductData(userId);
		Assert.assertEquals(TestConstants.testData.get("selectedItemName"), productData.get(0));
		Assert.assertEquals(TestConstants.testData.get("selectedItemDescription"), productData.get(1));
	}

	public void placeOrderForCartItem() {
		Assert.assertTrue(isElementExists(placeOrderBtn));
		clickOnElement(placeOrderBtn, "clicking on place order button");
		Assert.assertTrue(isElementExists(selectPaymentOptionText, TestConstants.TIMEOUT_PERIOD_LONG));
		clickOnElement(cashPaymentRadioButton, "selecting cash as payment option");
		clickOnElement(paymentPagePlaceOrderButton, "clicking on place order button");
		Assert.assertTrue(isElementExists(orderSuccessfullText), "Error occured while placing an order");
		String refNum = getElementText(orderReferenceNum);
		TestConstants.testData.put("orderReferenceNum", refNum.substring(refNum.lastIndexOf(" ")+1));
	}

	public void verifyPlacedOrderWithDB() {
		Assert.assertEquals(TestConstants.testData.get("userId").toString(), 
				ObjectRepository.dataBaseUtil.getOrderUserId(TestConstants.testData.get("orderReferenceNum").toString()), 
				"orderId is not reflected in the database");
	}
	

}
