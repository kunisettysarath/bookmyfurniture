package com.bookmyfurniture.ui.steps;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.bookmyfurniture.common.PropertyUtil;
import com.bookmyfurniture.common.Reporter;
import com.bookmyfurniture.common.TestConstants;
import com.bookmyfurniture.service.utils.CommonUtils;
import com.bookmyfurniture.ui.generalLibraries.ObjectRepository;
import com.bookmyfurniture.ui.pages.HomePage;

public class HomeSteps extends HomePage {

	WebDriver driver = null;
	Reporter report = null;

	public HomeSteps(WebDriver driver, Reporter report) {
		super(driver, report);
	}

	HomePage homePage = ObjectRepository.homePageObj;

	public void loginInToApp() {
		registerInToApp("TeepUs", "9868474432", "d21t@sdf.sdfs", "Test@1234");
	}
	
	public void registerRandomTopApp() {
		CommonUtils commonUtils = ObjectRepository.commonUtils;
		String userName = commonUtils.randomStringGenerator(10, "text");
		String password = "@"+commonUtils.randomStringGenerator(10, "alphaNumeric");
		String number = commonUtils.randomStringGenerator(10, "number");
		String email = commonUtils.randomStringGenerator(10, "alphaNumeric") + "@gmail.com";
		clickOnElement(signInButton, "Clicking on the home page signIn button");
		isElementExists(signInpageHeader, "verifying " + signInButton + " element");
		clickOnElement(createAccountButton, "clicking on create account button");
		sendTextToElement(userNameInputField, userName, "sending text \"" + userName + "\" to username field");
		TestConstants.testData.put("registerUserName", userName);
		sendTextToElement(mobileNoInputField, number, "sending text \"" + number + "\" to mobile number field");
		TestConstants.testData.put("registerUserMobNo", number);
		sendTextToElement(emailIdInputField, email, "sending text \"" + email + "\" to email field");
		TestConstants.testData.put("registerUserEmailId", email);
		sendTextToElement(passwordInputField, password, "sending text \"" + password + "\" to password field");
		TestConstants.testData.put("registerUserPassword", password);
		clickOnElement(registerButton, "clicking on register button");
	}

	public void registerInToApp(String userName, String number, String email, String password) {
		clickOnElement(signInButton, "Clicking on the home page signIn button");
		isElementExists(signInpageHeader, "verifying " + signInButton + " element");
		clickOnElement(createAccountButton, "clicking on create account button");
		sendTextToElement(userNameInputField, userName, "sending text \"" + userName + "\" to username field");
		TestConstants.testData.put("registerUserName", userName);
		sendTextToElement(mobileNoInputField, number, "sending text \"" + number + "\" to mobile number field");
		TestConstants.testData.put("registerUserMobNo", number);
		sendTextToElement(emailIdInputField, email, "sending text \"" + email + "\" to email field");
		TestConstants.testData.put("registerUserEmailId", email);
		sendTextToElement(passwordInputField, password, "sending text \"" + password + "\" to password field");
		TestConstants.testData.put("registerUserPassword", password);
		clickOnElement(registerButton, "clicking on register button");
	}

	public void signInWithRegisteredUser() {
		navigateTo(PropertyUtil.getData("websiteURL"));
		clickOnElement(signInButton, "Clicking on the home page signIn button");
		isElementExists(signInpageHeader);
		sendTextToElement(signInEmailField, TestConstants.testData.get("registerUserEmailId").toString(),
				"sending text \"" + TestConstants.testData.get("registerUserEmailId") + "\" to email field");
		sendTextToElement(signInPasswordField, TestConstants.testData.get("registerUserPassword").toString(),
				"sending text \"" + TestConstants.testData.get("registerUserPassword") + "\" to password field");
		clickOnElement(signInSubmitButton, "clicking on sign in button");
		isElementExists(browserFromCategoryText);
		isElementExists(loggedInMessage);
		Assert.assertTrue(
				getElementText(loggedInMessage)
				.equalsIgnoreCase("Hi, " + TestConstants.testData.get("registerUserName").toString()),
				"user name not present in the home page");
	}
	
	
	public void signInWithRegisteredUser(String email, String pass) {
		navigateTo(PropertyUtil.getData("websiteURL"));
		clickOnElement(signInButton, "Clicking on the home page signIn button");
		isElementExists(signInpageHeader);
		sendTextToElement(signInEmailField, email,
				"sending text \"" + email + "\" to email field");
		sendTextToElement(signInPasswordField, pass,
				"sending text \"" + pass + "\" to password field");
		clickOnElement(signInSubmitButton, "clicking on sign in button");
		isElementExists(browserFromCategoryText);
		isElementExists(loggedInMessage);
		TestConstants.testData.put("userEmail", email);
	}
}
