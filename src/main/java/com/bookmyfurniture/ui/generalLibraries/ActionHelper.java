package com.bookmyfurniture.ui.generalLibraries;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.bookmyfurniture.common.Reporter;
import com.bookmyfurniture.common.TestConstants;

public class ActionHelper {

	WebDriver driver;
	Reporter report;
	Logger log = Logger.getLogger(ActionHelper.class);

	public ActionHelper(WebDriver driver, Reporter report) {
		this.driver = driver;
		this.report = report;
	}

	protected void selectBoxByVisibleText(WebElement element, String data) {
		waitForElement(element, 1);

		try {
			Select select = new Select(element);
			select.selectByVisibleText(data);
			report.stepPassed("selecting " + data + " on the element:->" + element);
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
		}

	}

	protected void selectBoxByValue(WebElement element, String data) {
		try {
			Select select = new Select(element);
			select.selectByValue(data);
			report.stepPassed("clicking on the element:->" + element);
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	protected void clickOnElement(WebElement element, int timeOut, String message) {
		waitForElement(element, timeOut);
		try {
			log.info("clicking on element----->"+element);
			element.click();
			report.stepPassed(message); // !!!!after doing an action the element
			// getting changed!!!!!
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	protected void clickOnElement(WebElement element, String message) {
		waitForElement(element);
		try {
			log.info("clicking on element----->"+element);
			element.click();
			report.stepPassed(message); // !!!!after doing an action the element
										// getting changed!!!!!
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	protected void clickOnElement(WebElement element, int timeOut) {
		waitForElement(element, timeOut);
		try {
			log.info("clicking on element----->"+element);
			element.click();
			report.stepPassed("clicking on the element:->" + element); 
			// !!!!after doing an action the element getting changed!!!!!
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	protected void clickOnElement(WebElement element) {
		report.stepPassed("clicking on the element:->" + element);
		try {
			log.info("clicking on element----->"+element);
			element.click();
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	protected void sendTextToElement(WebElement element, String data, int timeOut, String message) {
		waitForElement(element, timeOut);
		element.click();
		try {
			log.info("sending text to element----->"+element);
			element.sendKeys(data);
			report.stepPassed(message);
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	protected void sendTextToElement(WebElement element, String data, int timeOut) {
		waitForElement(element, timeOut);
		element.click();
		try {
			log.info("sending text to element----->"+element);
			element.sendKeys(data);
			report.stepPassed("text sent to element:->" + element);
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	protected void sendTextToElement(WebElement element, String data) {
		try {
			log.info("sending text to element----->"+element);
			element.sendKeys(data);
			report.stepPassed("text sent to element:->" + element);
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	protected void sendTextToElement(WebElement element, String data, String message) {
		try {
			log.info("sending text to element----->"+element);
			element.sendKeys(data);
			report.stepPassed("text sent to element:->" + element);
		} catch (Exception e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	protected String getElementText(WebElement element, int timeOut) {
		waitForElement(element, timeOut);
		log.info("\"" + element.getText() + "\" is the text obtained from element -->" + element);
		return element.getText();
	}
	
	protected String getElementText(WebElement element) {
		waitForElement(element);
		log.info("\"" + element.getText() + "\" is the text obtained from element -->" + element);
		return element.getText();
	}
	
	protected boolean isElementExists(WebElement element, String message) {
		waitForElement(element);
		report.stepPassed(message);
		log.info("element found -->" + element);
		return element.isDisplayed();
	}
	
	protected boolean isElementExists(WebElement element, int timeOut, String message) {
		waitForElement(element, timeOut);
		report.stepPassed(message);
		log.info("element found -->" + element);
		return element.isDisplayed();
	}
	
	protected boolean isElementExists(WebElement element) {
		waitForElement(element);
		report.stepPassed("Element found--->"+element);
		log.info("element found -->" + element);
		return element.isDisplayed();
	}

	protected boolean isElementExists(WebElement element, int timeOut) {
		waitForElement(element, timeOut);
		report.stepPassed("Element found--->"+element);
		log.info("element found -->" + element);
		return element.isDisplayed();
	}

	protected void navigateTo(String url) {
		log.info("website url navigating to "+url);
		driver.navigate().to(url);
	}

	protected void waitForElement(WebElement element) {
		log.info("waiting for element---->" + element);
		try {
			WebDriverWait wait = new WebDriverWait(driver, TestConstants.TIMEOUT_PERIOD_LONG);
			wait.until(ExpectedConditions.visibilityOf(element));
			log.info("element found--->" + element);
		} catch (TimeoutException e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	protected void waitForElementNotVisible(WebElement element) {
		log.info("waiting for element to be invisible---->" + element);
		try {
			WebDriverWait wait = new WebDriverWait(driver, TestConstants.TIMEOUT_PERIOD_MEDIUM);
			if(element.isDisplayed())
				wait.until(ExpectedConditions.invisibilityOf(element));
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	protected void waitForElement(WebElement element, int timeOut) {
		log.info("waiting for element---->" + element);
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			wait.until(ExpectedConditions.visibilityOf(element));
			log.info("element found--->" + element);
		} catch (TimeoutException e) {
			log.info(ExceptionUtils.getStackTrace(e));
			report.testFailed(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

}
