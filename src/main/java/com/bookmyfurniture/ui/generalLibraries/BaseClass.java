package com.bookmyfurniture.ui.generalLibraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.bookmyfurniture.common.PropertyUtil;
import com.bookmyfurniture.common.Reporter;
import com.bookmyfurniture.common.TestConstants;
import com.bookmyfurniture.ui.utils.ExcelUtil;
import com.bookmyfurniture.ui.utils.GmailReporting;
import com.bookmyfurniture.ui.utils.ScreenshotUtil;

public class BaseClass {

	private static WebDriver driver = null;
	private static Reporter report = null;
	ScreenshotUtil screenshotUtil = null;
	GmailReporting gmailReporting = new GmailReporting();
	Logger log = Logger.getLogger(BaseClass.class);
	static String testType=null;

	public void initializeTestReporting(String testName) {
		report.startTest(testName);
		log.info("testcase "+testName+" initialized for reporting");
	}

	public void startNode(String testName) {
		report.createNode(testName);

	}

	public void stopNode() {
		report.killNode();
	}
	
	@DataProvider(name="registerUserData")
	public Object[][] dataProviderMethod(){
		return new ExcelUtil("registerUserDataExcel", "RegisterData").getTotalExcelData();
	}

	@BeforeSuite(alwaysRun=true)
	public void beforeSuit() {
		report = new Reporter();
		report.configReporter();
		log.info("configuring reports");
	}

	@BeforeMethod(alwaysRun=true)
	public void beforeTest() {
		System.setProperty("testType", "ui");
		System.setProperty("browser", "chrome");
		testType = System.getProperty("testType");
		if (!testType.equalsIgnoreCase("service")) {
			report.addExtraInfoToReport("Os", "Windows");
			report.addExtraInfoToReport("Browser", System.getProperty("browser"));
			log.info("browser type:  "+System.getProperty("browser"));
			log.info("test type:  "+System.getProperty("browser"));
			driver = new InitializeBrowser(driver).getDriver(System.getProperty("browser"));
			screenshotUtil = new ScreenshotUtil(driver);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(TestConstants.TIMEOUT_PERIOD_MEDIUM, TimeUnit.SECONDS);
			log.info(PropertyUtil.getData("websiteURL")+" website launching");
			driver.get(PropertyUtil.getData("websiteURL"));
		}
		new ObjectRepository(driver, report).initializeClasses();
		log.info("Object repository instantiated");
	}

	@AfterMethod(alwaysRun=true)
	public void testListeners(ITestResult result) throws IOException {
		if (ITestResult.FAILURE == result.getStatus()) {
			try {
				log.error(result.getThrowable().getMessage());
				if(!testType.equalsIgnoreCase("service"))
					report.testFailed("TEST FAILED ===> " + result.getName() + " | REASON ===> " + result.getThrowable().getMessage());
				else
					report.serviceStepFailed("TEST FAILED ===> " + result.getName() + " | REASON ===> " + result.getThrowable().getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (ITestResult.SUCCESS == result.getStatus()) {
			try {
				log.info(result.getName() + " TEST PASSED");
				if(!testType.equalsIgnoreCase("service"))
					report.stepPassed("TEST PASSED ===> " + result.getName());
				else
					report.serviceStepPassed("TEST PASSED ===> " + result.getName());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		else if (ITestResult.SKIP == result.getStatus()) {
			try {
				log.info(result.getName() + " TEST SKIPPED");
				if(!testType.equalsIgnoreCase("service"))
					report.testFailed("TEST SKIPPED ===> " + result.getName() + " | REASON ===> " + result.getThrowable().getMessage());
				else
					report.serviceStepFailed("TEST SKIPPED ===> " + result.getName() + " | REASON ===> " + result.getThrowable().getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		if(!testType.equalsIgnoreCase("service"))
			driver.close();
	}

	@AfterSuite(alwaysRun=true)
	public void aSuit() {
		if(!testType.equalsIgnoreCase("service"))
			driver.quit();
		log.info("wrapping up with driver and reporting");
		report.stopReporting();
		gmailReporting.sendMailOnRequest();
	}
}



/*try {
freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
} catch (ClassNotFoundException e) {
e.printStackTrace();
}*/