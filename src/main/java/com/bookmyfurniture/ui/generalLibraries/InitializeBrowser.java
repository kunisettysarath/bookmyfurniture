package com.bookmyfurniture.ui.generalLibraries;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.bookmyfurniture.common.PropertyUtil;

public class InitializeBrowser {
	protected WebDriver driver = null;
	public InitializeBrowser(WebDriver driver) {
		// TODO Auto-generated constructor stub
		driver = this.driver;
	}

	public WebDriver getDriver(String browserType) {
		if (browserType.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", PropertyUtil.getData("chromeDriverPath"));
			driver = new ChromeDriver();
		} else if (browserType.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", PropertyUtil.getData("ieDriverPath"));
			driver = new InternetExplorerDriver();

		}
		return driver;
	}

}
