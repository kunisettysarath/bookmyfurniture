package com.bookmyfurniture.ui.generalLibraries;

import org.openqa.selenium.WebDriver;

import com.bookmyfurniture.common.Reporter;
import com.bookmyfurniture.service.register.RegisterService;
import com.bookmyfurniture.service.utils.CommonUtils;
import com.bookmyfurniture.ui.pages.AllFurniturePage;
import com.bookmyfurniture.ui.pages.HomePage;
import com.bookmyfurniture.ui.steps.AllFurnitureSteps;
import com.bookmyfurniture.ui.steps.HomeSteps;
import com.bookmyfurniture.ui.utils.DataBaseUtil;

public class ObjectRepository {

	// ****** static variables will be initialized before constructor gets executed ******
	static WebDriver driver =null;
	static Reporter report =null;
	public ObjectRepository(WebDriver driver, Reporter report){
		ObjectRepository.driver = driver;
		ObjectRepository.report = report;
	}
	public static HomePage homePageObj;
	public static HomeSteps homeStepsObj;
	public static AllFurniturePage allFurniturePageObj;
	public static AllFurnitureSteps allFurnitureStepsObj;
	public static RegisterService registerServiceObj;
	public static DataBaseUtil dataBaseUtil;
	public static CommonUtils commonUtils;
	
	public void initializeClasses(){
		homePageObj = new HomePage(driver, report);
		homeStepsObj = new HomeSteps(driver, report);
		allFurniturePageObj = new AllFurniturePage(driver, report);
		allFurnitureStepsObj = new AllFurnitureSteps(driver, report);
		registerServiceObj = new RegisterService(report);
		dataBaseUtil = new DataBaseUtil();
		commonUtils = new CommonUtils();
	}

}
